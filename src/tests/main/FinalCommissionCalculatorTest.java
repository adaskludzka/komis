package main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class FinalCommissionCalculatorTest
{
    private CommissionCalculator commissionCalculator;

    @Test
    void forZeroCommissionShouldReturnSamePrice()
    {
        commissionCalculator = new FinalCommissionCalculator(0);

        BigDecimal price = new BigDecimal(100.5);
        Car car = new Car(1,"Audi","A8",price,200000,100);

        BigDecimal actual = commissionCalculator.count(car);
        BigDecimal expected = price;

        Assertions.assertEquals(expected,actual);
    }

    @Test
    void commissionMustBePositive()
    {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new FinalCommissionCalculator(-1)
        );
    }

    @Test
    void commissionCalculatorWorksCorrectly()
    {
        double commission = 0.2;
        commissionCalculator = new FinalCommissionCalculator(commission);

        BigDecimal price = new BigDecimal(10.1);
        Car car = new Car(1,"Mercedes","GLA",price,170000,120);

        BigDecimal actual = commissionCalculator.count(car);
        BigDecimal expected = price.add(price.multiply(new BigDecimal(commission)));


        assertEquals(expected.doubleValue(),actual.doubleValue(),0.01);
        //Assertions.assertEquals(expected,actual,);
    }
}