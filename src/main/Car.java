package main;

import java.math.BigDecimal;

public class Car {

    private int id;
    private String brand;
    private String model;
    private BigDecimal price;
    private int mileage;
    private int power;


    public int getId()
    {
        return id;
    }

    public Car(int id, String brand, String model, BigDecimal price, int mileage, int power)
    {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.mileage = mileage;
        this.power = power;
    }

    public String getBrand()
    {
        return brand;
    }

    public String getModel()
    {
        return model;
    }

    public BigDecimal getPrice()
    {
        return price;
    }

    public int getMileage()
    {
        return mileage;
    }

    public int getPower()
    {
        return power;
    }

    public Car addCommission(BigDecimal commission)
    {
        return new Car(id, brand,model,price.add(commission),mileage,power);
    }
}