package main;

import java.math.BigDecimal;

public class FinalCommissionCalculator implements CommissionCalculator
{
    private double commission;

    public FinalCommissionCalculator(double commission)
    {
        if(commission < 0)
            throw new IllegalArgumentException("Prowizja nie moze byc ujemna");
        this.commission = commission;
    }

    @Override
    public BigDecimal count(Car car)
    {
        if(car == null)
            throw new NullPointerException();
        return car.getPrice().multiply(new BigDecimal(1 + commission));
    }
}
