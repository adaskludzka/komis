package main;

import java.util.List;
import java.util.Optional;

public class Komis
{
    private List<Car> cars;
    private CommissionCalculator commissionCalculator;

    public Komis(CommissionCalculator commissionCalculator)
    {
        this.commissionCalculator = commissionCalculator;
    }

    public void add(Car car)
    {
        cars.add(car);
    }

    public List<Car> currentOffer()
    {
        return cars.stream().map(car -> car.addCommission(commissionCalculator.count(car))).toList();
    }

    public void buy(Car car)
    {
        Optional<Car> first = cars.stream().filter(c -> c.getId() == car.getId()).findFirst();
        cars.remove(first.orElseThrow());
    }
}
