package main;

import java.math.BigDecimal;

public interface CommissionCalculator
{
    BigDecimal count(Car car);
}
